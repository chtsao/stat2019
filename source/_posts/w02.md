---
title: Week 2. From x's to X
date: 2019-03-04 18:24:50
tags: class note
---
#### The Big Question
統計學這門課再討論什麼問題呢？簡單來說, 就是如何由樣本來推論母體。以符號來說概念上更清楚。
我們會希望由 $$ x_1, \cdots, x_n \rightarrow X.$$
但事實上，我們僅能探討
$$ X_1, \cdots, X_n \rightarrow X.$$
其中\\(X_1, \cdots, X_n \sim_{iid}\\) 來自某一分配。

