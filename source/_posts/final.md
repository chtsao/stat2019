---
title: Finally, the Final
date: 2019-06-08 05:58:22
tags: [final, etude]
---
![Running](http://dirtylooks.co.uk/wp-content/uploads/2011/10/chariots_still_wide2_720-790x444.jpg)
## Final Exam
* 日期: 2019 0618 (二)
* 時間: 1310-1440. 
* 地點:  A210 
* 範圍：上課及習題內容。
* 其他：No cheatsheet nor mobile phone. Prepare early and Good Luck!


提早準備，固實會的，加強生疏的，弄懂原來不會的！----考試不難，會就簡單！

### [練習題](http://faculty.ndhu.edu.tw/~chtsao/edu/19/stat/Etude4Fin.pdf)
這是之前承諾要給各位的[練習題](http://faculty.ndhu.edu.tw/~chtsao/edu/19/stat/Etude4Fin.pdf)。為了把可能考得一些主題都放上去，題目比較多些。建議各位先讀讀上課筆記，複習Homework題目。都弄懂之後，找個大約120分鐘的空檔時間，當作考試一樣來做。我們會在6/13檢討部份內容。Good Luck!
**新增** 另外，Beroulli p 的信賴區間與檢定練習問題請參考[練習題2](http://faculty.ndhu.edu.tw/~chtsao/edu/19/stat/etude4final2.pdf)。