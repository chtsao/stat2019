---
title: Week 6-1. ML II
date: 2019-03-26 20:46:17
tags: [ maximum likelihood estimator, Bernoulli, Normal ]
---
[![Deadmentheme](https://static.rogerebert.com/uploads/review/primary_image/reviews/dead-man-1996/hero_EB19960628REVIEWS606280301AR.jpg)](https://youtu.be/vyM4h4iVeFo)
### Steps for deriving MLE 
a. Given \\( X =x\\), write down the likelihood function \\( L(\theta| x)\\)
b. Derive MLe (estimate) which maxmizes \\( L(\theta|x)\\). That is
	 $$ \hat{\theta}(x) = arg \max_{\theta \in \Theta} L(\theta|x) $$ or equivalently 
$$ L(\hat{\theta}(x)|x) \geq L(\theta|x), \quad for\  all\  \theta.$$
**Note**: 不要亂微，看清 \\( \Theta\\).
c. MLe (estimate) \\( \leadsto \\) MLE (estimator): \\( \hat{\theta}(x) \leadsto \hat{\theta}(X).\\)

### Bernoulli
Let \\(  X_1, \cdots, X_n \sim_{iid} \\) Bernoulli(p), \\( p \in \Theta=[0,1]\\). Then  \\( \hat{p}(X)\\), MLE for p, is   \\( \bar{X}\\). 

*Note* that if  \\( \Theta\\) is not an interval, say \\( \Theta=\\) {\\( p_0, p_1, p_3\\)}  for some points    \\( p_0, p_1, p_3 \in [0,1]\\) then MLe/MLE should be derived from definition, 不要亂微。

### Normal
Let \\(  X_1, \cdots, X_n \sim_{iid} N(\mu, \sigma^2) \\) .
1. For estimating \\( \mu, \mu \in \Theta=\mathcal{R}\\) and  \\( \sigma^2\\) is unknown. The MLE is \\( \hat{\mu}(X)= \bar{X}.\\)
2. For estimating \\( (\mu, \sigma^2)\\) both unknown and \\( (\mu, \sigma^2) \in \mathcal{R}\times (0, \infty)\\). The MLE is
$$ (\hat{\mu}(X), \hat{\sigma^2}(X)) =( \bar{X}, \frac{1}{n}\sum_{i=1}^n (X_i - \bar{X})^2). $$

*Note* that if  \\( \Theta\\) is not an interval, say \\( \Theta=\\) is a collection of some finite discrete points then MLe/MLE should be  derived from definition, 不要亂微。
 3. (Exercise) For estimating \\( \sigma^2\\) with known \\( \mu\\) and  \\( \sigma^2 \in \Theta=(0, \infty).\\) The MLE is 
 $$ \hat{\sigma^2}(X)= \frac{1}{n}\sum_{i=1}^n (X_i - \mu )^2$$.  
